/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import pt.ipp.isep.dei.esoft.gpsd.adapter.AtuaEm;
import java.util.List;

/**
 *
 * @author Diogo Oliveira
 */
public interface ServicoExterno {
    
    public List <AtuaEm> obtemAtuacao(CodigoPostal base, double raio);

    public List<AtribuicaoPedido> atribuicaoPedidos(List<PedidoPrestacaoServico> listaPedidosTotaisSubmetidos);
}
