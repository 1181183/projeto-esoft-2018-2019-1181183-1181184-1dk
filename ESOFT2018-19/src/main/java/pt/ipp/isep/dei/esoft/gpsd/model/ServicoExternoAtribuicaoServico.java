/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.List;

/**
 *
 * @author Diogo Oliveira
 */
public interface ServicoExternoAtribuicaoServico {
    
    public List<AtribuicaoPedido> atribuicaoPedidos(List<PedidoPrestacaoServico> listaPedidosTotais);
}
