
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.Objects;


public class AreaGeografica {

    private String localizacao;
    private float raio;
    private String custo;

    public AreaGeografica(String localizacao, String custoLocal, String raio) {
        // valida que os valores necessários para criar categoria são todos válidos

        if ((localizacao == null) || (custoLocal == null) || (raio == null ))                      {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }else 
        // guarda a informação da categoria que está a ser criada
        {
            this.localizacao = localizacao;
            this.custo = custo;
        }   
    }   

}
