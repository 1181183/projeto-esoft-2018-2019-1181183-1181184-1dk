/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author tarde
 */
public class CandidaturaPrestadorDeServicos {

    private String m_strNif;
    private long m_nMecanografico;
    private String m_strNomeComp;
    private String m_strNomeAbv;
    private String m_strTel;
    private String m_strEMail;
    private List<EnderecoPostal> m_lstMoradas = new ArrayList<EnderecoPostal>();
    private List<HabilitacaoAcademica> m_lstHabAcad = new ArrayList<HabilitacaoAcademica>();
    private List<HabilitacaoProfissional> m_lstHabProf = new ArrayList<HabilitacaoProfissional>();
    private List<DocumentosComprovativos> m_lstDocComp = new ArrayList<DocumentosComprovativos>();
    private List<String> m_lstCatId = new ArrayList<String>();

    public CandidaturaPrestadorDeServicos(String strNif, long nMecanografico, String strNomeComp, String strNomeAbv, String strTel, String strEMail, EnderecoPostal oMorada, HabilitacaoAcademica aHabAcademica, HabilitacaoProfissional aHabProfissional, DocumentosComprovativos oDocComprovativo, String catId) {
        if ((strNomeComp == null) || (strNomeAbv == null) || (strNif == null) || (strTel == null)
                || (strEMail == null) || (oMorada == null) || (nMecanografico < 0)
                || (strNomeComp.isEmpty()) || (strNomeAbv.isEmpty()) || (strNif.isEmpty()) || (strTel.isEmpty())
                || (strEMail.isEmpty())) {
            throw new IllegalArgumentException("Apenas as habilitações profissionais, académicas e documentos comprovativos podem estar em branco!!");
        }
        this.m_strNif = strNif;
        this.m_nMecanografico = nMecanografico;
        this.m_strNomeComp = strNomeComp;
        this.m_strNomeAbv = strNomeAbv;
        this.m_strTel = strTel;
        this.m_strEMail = strEMail;
        m_lstMoradas.add(oMorada);
        m_lstHabAcad.add(aHabAcademica);
        m_lstHabProf.add(aHabProfissional);
        m_lstDocComp.add(oDocComprovativo);
        m_lstCatId.add(catId);
        
    }
    
    public String getNomeCompleto() {
        return this.m_strNomeComp;
    }
    
    public String getNomeAbreviado() {
        return this.m_strNomeAbv;
    }
    
    public long getNumeroMecanografico() {
        return this.m_nMecanografico;
    }
    
    public String getNif() {
        return this.m_strNif;
    }
    public String getEmail() {
        return this.m_strEMail;
    }

    public boolean hasEmail(String strEmail) {
        return this.m_strEMail.equalsIgnoreCase(strEmail);
    }
    
    public boolean hasNif(String strNif){
        return this.m_strNif.equalsIgnoreCase(strNif);
    }

    public boolean addEnderecoPostal(EnderecoPostal oMorada) {
        return this.m_lstMoradas.add(oMorada);
    }

    public static EnderecoPostal novoEnderecoPostal(String end, String codPostal, String localidade) {
        return new EnderecoPostal(end, codPostal, localidade);
    }

    public boolean removeEnderecoPostal(EnderecoPostal oMorada) {
        return this.m_lstMoradas.remove(oMorada);
    }

    public boolean addHabilitacaoAcademica(HabilitacaoAcademica aHabilitacaoAcademica) {
        return this.m_lstHabAcad.add(aHabilitacaoAcademica);
    }

    public boolean removeHabilitacaoAcademica(HabilitacaoAcademica aHabilitacaoAcademica) {
        return this.m_lstHabAcad.remove(aHabilitacaoAcademica);
    }

    public boolean addHabilitacaoProfissional(HabilitacaoProfissional aHabilitacaoProfissional) {
        return this.m_lstHabProf.remove(aHabilitacaoProfissional);
    }

    public boolean removeHabilitacaoProfissional(HabilitacaoProfissional aHabilitacaoProfissional) {
        return this.m_lstHabProf.remove(aHabilitacaoProfissional);
    }

    public boolean addDocumentoComprovativo(DocumentosComprovativos oDocComprovativo) {
        return this.m_lstDocComp.add(oDocComprovativo);
    }

    public boolean removeDocumentoComprovativo(DocumentosComprovativos oDocComprovativo) {
        return this.m_lstDocComp.add(oDocComprovativo);
    }

    public static HabilitacaoAcademica novaHabilitacaoAcademica() {
        return new HabilitacaoAcademica();
    }

    public static HabilitacaoProfissional novaHabilitacaoProfissional() {
        return new HabilitacaoProfissional();
    }

    public static DocumentosComprovativos novoDocumentoComprovativo() {
        return new DocumentosComprovativos();
    }

    @Override
    public String toString() {
        String str = String.format("%s - %s - %s - %s", this.m_lstHabAcad, this.m_lstHabProf, this.m_lstDocComp);
        return str;
    }

}
