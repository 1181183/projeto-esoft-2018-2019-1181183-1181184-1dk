/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Diogo Oliveira
 */
public class RegistoAreaGeografica {
    
    private List<AreaGeografica> m_lstAreasGeograficas;

    public RegistoAreaGeografica(ArrayList<AreaGeografica> m_lstAreasGeograficas) {
        this.m_lstAreasGeograficas = m_lstAreasGeograficas;
    }

    public RegistoAreaGeografica() {
        this.m_lstAreasGeograficas = new ArrayList<>();
        
    }
      
    public AreaGeografica getRegistoAreasGeograficas(AreaGeografica AreaGeo){
        return AreaGeo;
    }
    
    public List<AreaGeografica> getListaAreasGeograficas(){
        return m_lstAreasGeograficas;
    }
    
    public boolean addAreaGeografica(AreaGeografica ag) {
       return m_lstAreasGeograficas.add(ag);
    }

    public boolean registaAreaGeografica(AreaGeografica ag) throws Exception {
        if (validaAreaGeografica(ag)) {
            return addAreaGeografica(ag);
        } else {
            return false;
        }
    }
    
    public boolean validaAreaGeografica(AreaGeografica ag) {
        //Ainda não sabemos como validar a área geográfica...
        return true;
    }
    
}
