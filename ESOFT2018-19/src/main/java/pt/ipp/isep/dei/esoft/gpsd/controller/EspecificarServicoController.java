/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.gpsd.hclc.RegistoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistaTipoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoCategorias;
import pt.ipp.isep.dei.esoft.gpsd.model.Servico;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;


/**
 *
 * @author paulomaio
 */
public class EspecificarServicoController
{
    private Empresa m_oEmpresa;
    private RegistaTipoServico m_oRegistaTipoServico;
    private Servico m_oServico;
    private RegistoServico m_oRegistoServico;
    
    private String idTipo;
    private float duracao;
      
    private Set<Servico> m_lstServicos;
    
    public EspecificarServicoController()
    {
        if(!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO)){
            throw new IllegalStateException("Utilizador não Autorizado");
        }
    }
    
    public RegistaTipoServico getRegistaTipoServico(RegistaTipoServico RegistoTipoServicos){
        return RegistoTipoServicos;
    }
    
    public void setServico(String idTipo){
        this.idTipo = idTipo;
    }
    
    public RegistoCategorias getRegistoCategorias(RegistoCategorias RegistoCategorias){
        return RegistoCategorias;
    }
    
    public boolean novoServico(String strId, String strDescricaoBreve, String strDescricaoCompleta, double dCustoHora, String catId) {
        try
        {
            this.m_oServico = this.m_oRegistoServico.getRegistoServico(m_oServico);
            return this.m_oRegistaTipoServico.validaServico(m_oServico);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oServico = null;
            return false;
        }
    }
  
    public void setDuracaoPreDeterminada(float duracao){
        this.duracao = duracao;
    }
    
    public boolean registaServico() {
        return this.m_oEmpresa.registaServico(m_oServico);
    }

    public String getServicoString()
    {
        return this.m_oServico.toString();
    }

    public boolean validaServico(Servico oServico) {
        if (oServico.getId().equalsIgnoreCase(idTipo)){
            return true;
        }
        return false;
    }
    private boolean addServico(Servico oServico) {
        return m_lstServicos.add(oServico);
    }

}
