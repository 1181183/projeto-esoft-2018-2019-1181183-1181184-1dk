/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.gpsd.model.CandidaturaPrestadorDeServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.model.DocumentosComprovativos;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.HabilitacaoAcademica;
import pt.ipp.isep.dei.esoft.gpsd.model.HabilitacaoProfissional;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoCandidaturaAPrestadorServicos;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author tarde
 */
public class RegistarCandidaturaPrestadorServicosController {

    private Empresa m_oEmpresa;
    private CandidaturaPrestadorDeServicos m_oCandidaturaPrestadorServicos;
    private RegistoCandidaturaAPrestadorServicos registoCandidatura = this.m_oEmpresa.getRegistoCandidaturaAPrestadorServicos();

    public boolean novaCandidaturaPrestadorServicos( String strNif, long nMecanografico, String strNomeComp, String strNomeAbv, String strTel, String strEMail, String strEnd, String codPostal, String strLocalidade, String catId) {
        try {
            EnderecoPostal morada = CandidaturaPrestadorDeServicos.novoEnderecoPostal(strEnd, codPostal, strLocalidade);
            HabilitacaoAcademica habAcademica = CandidaturaPrestadorDeServicos.novaHabilitacaoAcademica();
            HabilitacaoProfissional habProfissional = CandidaturaPrestadorDeServicos.novaHabilitacaoProfissional();
            DocumentosComprovativos docComp = CandidaturaPrestadorDeServicos.novoDocumentoComprovativo();
            this.m_oCandidaturaPrestadorServicos = registoCandidatura.novaCandidaturaPrestadorServicos(strNif , nMecanografico, strNomeComp, strNomeAbv, strTel, strEMail, morada, habAcademica, habProfissional, docComp, catId);
            return registoCandidatura.validaCandidaturaPrestadorServiço(m_oCandidaturaPrestadorServicos);
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oEmpresa = null;
            return false;
        }
    }

    public boolean addEnderecoPostal(String end, String codPostal, String localidade) {
        if (this.m_oCandidaturaPrestadorServicos != null) {
            try {
                EnderecoPostal morada = CandidaturaPrestadorDeServicos.novoEnderecoPostal(end, codPostal, localidade);
                return this.m_oCandidaturaPrestadorServicos.addEnderecoPostal(morada);
            } catch (RuntimeException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return false;
    }

    public boolean addHabilitacaoAcademica() {
        if (this.m_oCandidaturaPrestadorServicos != null) {
            try {
                HabilitacaoAcademica habAcademica = CandidaturaPrestadorDeServicos.novaHabilitacaoAcademica();
                return this.m_oCandidaturaPrestadorServicos.addHabilitacaoAcademica(habAcademica);
            } catch (RuntimeException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return false;
    }

    public boolean addHabilitacaoProfissional() {
        if (this.m_oCandidaturaPrestadorServicos != null) {
            try {
                HabilitacaoProfissional habProfissional = CandidaturaPrestadorDeServicos.novaHabilitacaoProfissional();
                return this.m_oCandidaturaPrestadorServicos.addHabilitacaoProfissional(habProfissional);
            } catch (RuntimeException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return false;
    }

    public boolean addDocComp() {
        if (this.m_oCandidaturaPrestadorServicos != null) {
            try {
                DocumentosComprovativos doc = CandidaturaPrestadorDeServicos.novoDocumentoComprovativo();
                return this.m_oCandidaturaPrestadorServicos.addDocumentoComprovativo(doc);
            } catch (RuntimeException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return false;
    }

    public boolean registaCandidatoPrestadorServicos() {
        return registoCandidatura.validaCandidaturaPrestadorServiço(this.m_oCandidaturaPrestadorServicos);
    }

    public List<Categoria> getCategorias() {
        return this.m_oEmpresa.getCategorias();
    }

    public String getPedidoServicoString() {
       return String.format("");
    }
}
