/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.Objects;

public class HabilitacaoProfissional {

    private String m_strHabProf;

    public HabilitacaoProfissional() {
// ainda não há informações sobre as informação a colocar na habilitação profissional
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.m_strHabProf);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o) {
            return true;
        }
        // null check
        if (o == null) {
            return false;
        }
        // type check and cast
        if (getClass() != o.getClass()) {
            return false;
        }
        // field comparison
        HabilitacaoProfissional obj = (HabilitacaoProfissional) o;
        return (Objects.equals(m_strHabProf, obj.m_strHabProf));
    }

    @Override
    public String toString() {
        return String.format(this.m_strHabProf);
    }

    boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
