/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author Diogo Oliveira
 */
public class AtribuicaoPedido {
    
    public int pedidoId;
    public Servico servico;
    public PrestadorServico prestadorServico;
    public DisponibilidadeDiaria dispDiaria;

    public AtribuicaoPedido(int pedidoId, Servico servico, PrestadorServico prestadorServico, DisponibilidadeDiaria dispDiaria) {
        this.pedidoId = pedidoId;
        this.servico = servico;
        this.prestadorServico = prestadorServico;
        this.dispDiaria = dispDiaria;
    }
    
    public int getPedidoId(){
        return this.pedidoId;
    }
    
    public Servico getServico() {
        return this.servico;
    }
    
    public PrestadorServico getPrestadorServico() {
        return this.prestadorServico;
    }
    
    public DisponibilidadeDiaria getDispDiaria() {
        return this.dispDiaria;
    }
}
