/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author Diogo Oliveira
 */
public class OrdemExecucao {
    
    private long id;
    private PrestadorServico prestador;
    private Servico servico;
    private DisponibilidadeDiaria dispDiaria;
    
    public OrdemExecucao(long id, PrestadorServico prestador, Servico servico, DisponibilidadeDiaria dispDiaria) {
        this.id = id;
        this.prestador = prestador;
        this.servico = servico;
        this.dispDiaria = dispDiaria;
    }
    
    public long getId(){
        return id;
    }
    
    public PrestadorServico getPrestador() {
        return prestador;
    }
    
    public Servico getServico() {
        return servico;
    }
    
    public DisponibilidadeDiaria getDisponibilidadeDiaria() {
        return dispDiaria;
    }
}
