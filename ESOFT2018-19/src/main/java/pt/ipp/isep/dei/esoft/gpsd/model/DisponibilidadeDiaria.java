/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author Rita
 */
public class DisponibilidadeDiaria {

    private String m_horaInicio;
    private String m_horaFim;

    public DisponibilidadeDiaria(String horaInicio, String horaFim) {
        this.m_horaInicio = horaInicio;
        this.m_horaFim = horaFim;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    public String toString() {
        return String.format("%s - %s ", this.m_horaInicio, this.m_horaFim);
    }

}
