/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.DecidirSobrePeriodoPropostoParaRealizacaoDeServicosController;
import pt.ipp.isep.dei.esoft.gpsd.controller.EspecificarAreaGeograficaController;
import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author Diogo Oliveira
 */
public class DecidirSobrePeriodoPropostoParaRealizacaoDeServicosUI {
    
   private DecidirSobrePeriodoPropostoParaRealizacaoDeServicosController m_controller;
   private long id;
   private String strEmail;
   private String estado;
   
   public DecidirSobrePeriodoPropostoParaRealizacaoDeServicosUI() {
        m_controller = new DecidirSobrePeriodoPropostoParaRealizacaoDeServicosController(strEmail, id);  
    }
    
    public void run() throws Exception
    {
        System.out.println("Pedido de Prestação de serviços:");
        apresentaDados();
        if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
            estado = Utils.EstadoPedido.PEDIDO_EXECUTAVEL;
            if (m_controller.setEstadoPedido(estado)) {
                System.out.println("Foi dada a ordem para a execução do pedido.");
                System.out.println(m_controller.getListaOrdensExecucao());
            } else {
                System.out.println("Não foi possivel concluir a ordem de execução do pedido.");
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    private void apresentaDados() 
    {
        System.out.println(m_controller.getAtribuicoesPedido());
    }
}
   
