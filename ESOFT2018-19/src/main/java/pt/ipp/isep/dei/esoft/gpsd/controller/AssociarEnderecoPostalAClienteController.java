/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.ListaEnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author Diogo Oliveira
 */
public class AssociarEnderecoPostalAClienteController {
    
    private ListaEnderecoPostal v_ListaEndPos;
    private EnderecoPostal v_EndPostal;
    
    public AssociarEnderecoPostalAClienteController(){
        if(!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_CLIENTE))
            throw new IllegalStateException("Utilizador não Autorizado");
    }
    
    public boolean novoEnderecoPostal (String local, String codPostal, String localidade){
        try{
            this.v_EndPostal = this.v_ListaEndPos.novoEnderecoPostal(local, codPostal, localidade);
            return true;
        }
        catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE , null , ex);
            this.v_EndPostal = null;
            return false;
        }
    }
    
    public boolean registaEnderecoPostal(){
        return this.v_ListaEndPos.validaEnderecoPostal(v_EndPostal);
    }
    
    public String getAreaGeograficaString(){
        return this.v_EndPostal.toString();
    }
}
