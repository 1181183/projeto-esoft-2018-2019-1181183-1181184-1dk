/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.hclc.RegistoPrestadorDeServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.ListaOrdemExecucao;
import pt.ipp.isep.dei.esoft.gpsd.model.OrdemExecucao;
import pt.ipp.isep.dei.esoft.gpsd.model.PrestadorServico;

/**
 *
 * @author Diogo Oliveira
 */
public class ConsultarOrdensDeExecucaoController {
    
    private Empresa empresa;
    private RegistoPrestadorDeServicos rps; 
    private PrestadorServico ps;
    
    public ConsultarOrdensDeExecucaoController(String email) {
        empresa = AplicacaoGPSD.getInstance().getEmpresa();
        rps = empresa.getRegistoPrestadorDeServicos();
        ps = rps.getPrestadorServicoByEmail(email);
    }
    
    public List<OrdemExecucao> getListaOrdemServicoDoPrestador(String dataInicio, String dataFim){
        return ps.getListaOrdemServico(dataInicio, dataFim);
    }
    
    public List<String> getListaFormatoExportacao() {
        return empresa.getListaFormatoExportacao();
    }
    
    public boolean valida(String dataInicio, String dataFim) {
        return ps.getListaOrdemExecucao().validaEnderecoPostal(dataInicio, dataFim);
    }
}
