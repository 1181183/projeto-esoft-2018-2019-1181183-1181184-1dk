/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author Diogo Oliveira
 */
public class RegistoCliente {

    List<Cliente> m_lstClientes;
    List<PedidoPrestacaoServico> m_lstTodosPedidosSubmetidos;
    private final AutorizacaoFacade m_oAutorizacao;

    public RegistoCliente() {
        m_lstClientes = new ArrayList<>();
        m_lstTodosPedidosSubmetidos = new ArrayList<>();
        this.m_oAutorizacao = new AutorizacaoFacade();

    }
    
    public Cliente getClienteByEmail(String strEMail) {
        for (Cliente cliente : this.m_lstClientes) {
            if (cliente.hasEmail(strEMail)) {
                return cliente;
            }
        }

        return null;
    }
    
    

    public Cliente novoCliente(String strNome, String strNIF, String strTelefone, String strEmail, EnderecoPostal morada, String codPostal, String end, String loc) {
        return new Cliente(strNome, strNIF, strTelefone, strEmail, morada, codPostal, end, loc);
    }

    public boolean registaCliente(Cliente oCliente, String strPwd) {
        if (this.validaCliente(oCliente, strPwd)) {
            if (this.m_oAutorizacao.registaUtilizadorComPapel(oCliente.getNome(), oCliente.getEmail(), strPwd, Constantes.PAPEL_CLIENTE)) {
                return addCliente(oCliente);
            }
        }
        return false;
    }

    private boolean addCliente(Cliente oCliente) {
        return m_lstClientes.add(oCliente);
    }

    public List<PedidoPrestacaoServico> getListaTodosPedidosSubmetidos() {
        for(Cliente cliente : m_lstClientes){
            if(!cliente.getPedidoSubmetido().equals(null)){
                addListaPedidosTotal(cliente.getPedidoSubmetido());
            }
        }
        return m_lstTodosPedidosSubmetidos;
    }
    
    public boolean addListaPedidosTotal(PedidoPrestacaoServico pedido){
        return m_lstTodosPedidosSubmetidos.add(pedido);
    }
    
    public boolean associarAtribuicaoAPedido(List<AtribuicaoPedido> listaAtribuicaoPedidos){
        for(Cliente cliente : m_lstClientes){
            for(PedidoPrestacaoServico pedido: cliente.getListaPrestacaoServicos().getListaPedidos()){
                for(AtribuicaoPedido atriPedido : listaAtribuicaoPedidos) {
                    if(atriPedido.getPedidoId() == pedido.getPedidoId()) {
                        return pedido.addAtribuicaoPedido(atriPedido);
                    }
                }
            }
        }
        return false;
    }
    
    public boolean validaCliente(Cliente oCliente, String strPwd) {
        boolean bRet = true;
        String strEmail = oCliente.getEmail();
        String strNif = oCliente.getNif();
            for (Cliente cliente : m_lstClientes) {
                if(cliente.getNif().equalsIgnoreCase(strNif) || cliente.getEmail().equalsIgnoreCase(strEmail)) {
                    bRet=false;
                }
            }

        if (this.m_oAutorizacao.existeUtilizador(oCliente.getEmail())) {
            bRet = false;
        }
        if (strPwd == null) {
            bRet = false;
        }
        if (strPwd.isEmpty()) {
            bRet = false;
        }
        //

        return bRet;
    }

}
