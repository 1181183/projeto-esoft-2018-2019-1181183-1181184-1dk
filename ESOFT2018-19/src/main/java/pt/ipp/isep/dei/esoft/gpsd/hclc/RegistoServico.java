/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.hclc;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.PrestadorServico;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistaTipoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.Servico;

/**
 *
 * @author Rita
 */
public class RegistoServico {

    List<Servico> m_lstServicos;
    private RegistaTipoServico m_regista;

    public RegistoServico() {
        m_lstServicos = new ArrayList<Servico>();
    }

    public List<Servico> getServicosDeCategoria() {
        List<Servico> ls = new ArrayList<>();
        ls.addAll(this.m_lstServicos);
        return ls;
    }

    public Servico getServicoById(String strId) {
        for (Servico serv : this.m_lstServicos) {
            if (serv.hasId(strId)) {
                return serv;
            }
        }

        return null;
    }
    
    public Servico getRegistoServico(Servico oServico){
        return oServico;
    }
}
