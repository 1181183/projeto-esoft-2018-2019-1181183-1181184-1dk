/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.IndicarDisponibilidadeDiariaController;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author Rita
 */
public class IndicarDisponibilidadeDiariaUI {

    private IndicarDisponibilidadeDiariaController m_controller;

    public IndicarDisponibilidadeDiariaUI() {
        m_controller = new IndicarDisponibilidadeDiariaController();
    }

    public void run() {
        System.out.println("\nIndicar Disponibilidade Diária:");

        if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaDisponibilidadeDiaria()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        String strHoraI = Utils.readLineFromConsole("Hora Início: ");
        String strHoraF = Utils.readLineFromConsole("Hora Fim: ");

        return m_controller.novoDisponibilidadeDiaria(strHoraI, strHoraF);
    }

    private void apresentaDados() {
        System.out.println("\nDisponibilidade Diária:\n" + m_controller.getDisponibilidadeDiariatoString());
    }

}
