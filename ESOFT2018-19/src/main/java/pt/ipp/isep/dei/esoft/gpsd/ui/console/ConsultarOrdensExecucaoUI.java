/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.ConsultarOrdensDeExecucaoController;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author Diogo Oliveira
 */
public class ConsultarOrdensExecucaoUI {
    
   private ConsultarOrdensDeExecucaoController m_controller;
   
   private String email;
   private String horaInicio;
   private String horaFim;
    public ConsultarOrdensExecucaoUI() {
        m_controller = new ConsultarOrdensDeExecucaoController(email);
    }
    
    public void run() throws Exception
    {
        apresentaDados();
        System.out.println("Indique o formato do ficheiro: ");
        m_controller.getListaFormatoExportacao();
        System.out.println("Selecione um dos tipos de ficheiro.");
        m_controller.valida(horaInicio, horaFim);
        
    }
    
    private void apresentaDados() 
    {
        System.out.println(m_controller.getListaOrdemServicoDoPrestador(horaInicio, horaFim));
    }
}
