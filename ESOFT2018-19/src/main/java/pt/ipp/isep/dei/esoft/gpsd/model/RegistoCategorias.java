/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;

/**
 *
 * @author Diogo Oliveira
 */
public class RegistoCategorias {
    
    private ArrayList<Categoria> m_lstCategorias;
    
    public ArrayList<Categoria> getCategorias(){
        return m_lstCategorias;
    }
    
        public Categoria getCategoriaById(String strId) {
        for (Categoria cat : this.m_lstCategorias) {
            if (cat.hasId(strId)) {
                return cat;
            }
        }

        return null;
    }
}
