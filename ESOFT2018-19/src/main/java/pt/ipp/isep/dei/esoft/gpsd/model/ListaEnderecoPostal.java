/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Diogo Oliveira
 */
public class ListaEnderecoPostal {

    private final List<EnderecoPostal>m_listaEnderecos;
    
    public ListaEnderecoPostal() {
        this.m_listaEnderecos = new ArrayList<>();
    }
   
    public EnderecoPostal novoEnderecoPostal(String local, String codPostal, String localidade){
        return new EnderecoPostal(local, codPostal, localidade);
    }

    public boolean addEnderecoPostal(EnderecoPostal endPostal){
        return m_listaEnderecos.add(endPostal);
    }
    
    public boolean removeEnderecoPostal(EnderecoPostal endPostal){
        return m_listaEnderecos.remove(endPostal);
    }
    
    public boolean validaEnderecoPostal(EnderecoPostal endPostal){
        boolean bRet = true;
        //ainda não sabemos como validar a disponibilidade diária

        return bRet;    
    }
    
    
}
