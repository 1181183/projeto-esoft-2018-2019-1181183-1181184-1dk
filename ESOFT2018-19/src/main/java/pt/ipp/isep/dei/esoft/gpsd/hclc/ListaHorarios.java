/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.hclc;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.model.PreferenciaHorario;

/**
 *
 * @author Rita
 */
public class ListaHorarios {

    private List<PreferenciaHorario> m_lstHorarios;

    public PreferenciaHorario novoPreferenciaHorario(String horaInicio, String dataInicio) {
        return new PreferenciaHorario(horaInicio, dataInicio);
    }

    public ListaHorarios() {
        this.m_lstHorarios = new ArrayList<PreferenciaHorario>();
    }

    public int countHorarios() {
        int ordem;
        return ordem = m_lstHorarios.size();
    }

    public boolean addHorario(PreferenciaHorario oHorario) {
        return this.m_lstHorarios.add(oHorario);
    }

    public boolean removeHorario(PreferenciaHorario oHorario) {
        return this.m_lstHorarios.remove(oHorario);
    }

    public boolean validaHorario(PreferenciaHorario oHorario) {
        boolean bRet = true;
        //ainda não sabemos como validar horário

        return bRet;
    }

}
