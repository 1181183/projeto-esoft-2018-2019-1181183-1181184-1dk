
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoAreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

public class EspecificarAreaGeograficaController {

    private Empresa v_emp;
    private RegistoAreaGeografica v_regAreaGeo;
    private AreaGeografica v_AreaGeo;
    
    public EspecificarAreaGeograficaController(){
        if(!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.v_emp = AplicacaoGPSD.getInstance().getEmpresa();
        
    }
    public boolean novaAreaGeografica(String localizacao, String custoLocal, String raio){
        try
        {
            this.v_AreaGeo = this.v_regAreaGeo.getRegistoAreasGeograficas(v_AreaGeo);
            return this.v_regAreaGeo.validaAreaGeografica(v_AreaGeo);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.v_AreaGeo = null;
            return false;
        }
    }
    
    public boolean registaAreaGeografica() throws Exception{
        return this.v_regAreaGeo.registaAreaGeografica(v_AreaGeo);
    }
    public String getAreaGeograficaString(){
        return  v_AreaGeo.toString();
    }
    
}
    
 
