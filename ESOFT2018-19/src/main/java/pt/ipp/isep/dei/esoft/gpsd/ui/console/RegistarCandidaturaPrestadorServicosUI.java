/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.controller.RegistarCandidaturaPrestadorServicosController;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author tarde
 */
public class RegistarCandidaturaPrestadorServicosUI {

    private RegistarCandidaturaPrestadorServicosController m_controller;

    public RegistarCandidaturaPrestadorServicosUI() {
        m_controller = new RegistarCandidaturaPrestadorServicosController();
    }

    public void run() {
        System.out.println("\nRegisto de Candidatura a Prestador de Serviços:");

        if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCandidatoPrestadorServicos()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        String strNif = Utils.readLineFromConsole("Nif: ");
        long nMecanografico = Long.parseLong(Utils.readLineFromConsole("Número Mecanográfico: "));
        String strNomeComp = Utils.readLineFromConsole("Nome Completo: ");
        String strNomeAbv = Utils.readLineFromConsole("Nome abreviado: ");
        String strTel = Utils.readLineFromConsole("Telefone: ");
        String strEMail = Utils.readLineFromConsole("EMail: ");
        String catId = Utils.readLineFromConsole("id categoria: ");
        
         System.out.println("\nIntroduza pelo menos um endereço postal:");
        int count = 0; 
        do
        {
   
            String strLocal = Utils.readLineFromConsole("Rua/Av.: ");
            String strCodPostal = Utils.readLineFromConsole("Cod. Postal: ");
            String strLocalidade = Utils.readLineFromConsole("Localidade: ");
            
            if (count == 0)
            {
                if(m_controller.novaCandidaturaPrestadorServicos(strNif, nMecanografico, strNomeComp, strNomeAbv, strTel, strEMail, strLocal, strCodPostal, strLocalidade, catId))
                {
                    count++;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if(!m_controller.addEnderecoPostal(strLocal, strCodPostal, strLocalidade))
                {
                    System.out.println("Ocorreu um erro ao adicionar o endereço.");
                }
            }    
        }while((count == 0) || Utils.confirma("Pretende introduzir outro Endereço Postal (S/N)?"));
        return true;
        //introduza pelo menos uma categoria
    }

    private void apresentaDados() {
        System.out.println("\n Cliente:\n" + m_controller.getPedidoServicoString());
    }
}
