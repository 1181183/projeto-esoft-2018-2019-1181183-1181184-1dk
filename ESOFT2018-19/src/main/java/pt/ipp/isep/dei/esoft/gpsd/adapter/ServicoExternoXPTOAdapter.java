/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.adapter;

import pt.ipp.isep.dei.esoft.gpsd.adapter.ServicoExternoXPTOAPI;
import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.model.CodigoPostal;

/**
 *
 * @author Diogo Oliveira
 */
public class ServicoExternoXPTOAdapter{
    
    public List<AtuaEm> obtemAtuacao(CodigoPostal codPostal, int raio){
        List<AtuaEm> listaDistancias = new ArrayList<>();
        for(String codigoPostalNaRegiao : ServicoExternoXPTOAPI.codigoDentroRegiao(codPostal.getCodigoPostal(), raio)){
            double distancia = ServicoExternoXPTOAPI.distanciaEntreRegioes(codPostal.getCodigoPostal(), codigoPostalNaRegiao);
            AtuaEm distanciaComoInstancia = new AtuaEm(distancia);
            listaDistancias.add(distanciaComoInstancia);
        }
        return listaDistancias;
    }
    
    
}
