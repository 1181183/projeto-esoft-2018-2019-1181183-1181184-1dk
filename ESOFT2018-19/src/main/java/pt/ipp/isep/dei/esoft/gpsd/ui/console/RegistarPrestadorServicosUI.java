/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.controller.RegistarPrestadorServicosController;
import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.CandidaturaPrestadorDeServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author Rita
 */
public class RegistarPrestadorServicosUI {
 
    private RegistarPrestadorServicosController m_controller;

    public RegistarPrestadorServicosUI() {
        m_controller = new RegistarPrestadorServicosController();
    }

    public void run() {
        System.out.println("\nRegistar Prestador de Serviços:");

        if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaPrestadorServico()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        String strNif = Utils.readLineFromConsole("Nif: ");
        if(m_controller.getCandidaturaByNif(strNif)){
            return true;
        }
        long longNumMecanografico = Long.parseLong(Utils.readLineFromConsole("Número Mecanográfico: "));
        String strNomeCompleto = Utils.readLineFromConsole("Nome Completo: ");
        String strNomeAbreviado = Utils.readLineFromConsole("Nome abreviado: ");
        String strEmailInstitucional = Utils.readLineFromConsole("Email institucional: ");
        String strpwd = Utils.readLineFromConsole("Password: ");
        
        List<Categoria> lc = m_controller.getRegistoCategoria().getCategorias();
        
        Categoria c;
        do {
        c = (Categoria) Utils.apresentaESeleciona(lc, "Selecione a categoria:");
        if (c != null) {
            m_controller.getRegistoCategoria().getCategorias().add(c);
        }
        } while (c!=null);
        
        AreaGeografica ag;
        do {
        ag = (AreaGeografica) Utils.apresentaESeleciona(lc, "Selecione a área geográfica:");
        if (ag != null) {
            m_controller.getRegistoAreaGeografica().addAreaGeografica(ag);
        }
        } while (c!=null);

        return m_controller.novoPrestadorServico(strNif, longNumMecanografico, strNomeCompleto, strNomeAbreviado, strEmailInstitucional, strpwd);
    }
    
    private void apresentaDados() {
        System.out.println("\nPrestador Serviços: \n" + m_controller.getPrestadorServicoString());
    }   
}
