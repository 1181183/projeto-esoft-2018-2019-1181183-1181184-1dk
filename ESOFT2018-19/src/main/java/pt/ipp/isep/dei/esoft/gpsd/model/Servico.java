/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.Objects;

/**
 *
 * @author paulomaio
 */
public class Servico {

    private String m_strId;
    private String m_strDescricaoBreve;
    private String m_strDescricaoCompleta;
    private double m_dCustoHora;
    private String m_oCategoria;
    
    private float duracao;

    public Servico(String strId, String strDescricaoBreve, String strDescricaoCompleta, double dCustoHora, String catId) {
        if ((strId == null) || (strDescricaoBreve == null) || (strDescricaoCompleta == null)
                || (dCustoHora < 0) || (catId == null)
                || (strId.isEmpty()) || (strDescricaoBreve.isEmpty()) || (strDescricaoCompleta.isEmpty()) ) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strId = strId;
        this.m_strDescricaoBreve = strDescricaoBreve;
        this.m_strDescricaoCompleta = strDescricaoCompleta;
        this.m_dCustoHora = dCustoHora;
    }

    public boolean hasId(String strId) {
        return this.m_strId.equalsIgnoreCase(strId);
    }

    public String getId() {
        return this.m_strId;
    }

    public boolean isDuracaoRequerida() {
        boolean bRet = true;

        //Escrever aqui de necessidade de inserir duração pelo cliente
        //
        return bRet;
    }
    
    public String calculoDuracao() {
        String duracao = "";
        //nao se sabe calcular a duração
        return duracao;
    }
    
    public boolean possuiDuracaoPreDeterminada(){
        if (m_strId.equalsIgnoreCase("Limitado") || m_strId.equalsIgnoreCase("Expansível")){
            return true;
        }
        return false;
    }
    
    public void setDuracaoPreDeterminada(float duracao){
        if (possuiDuracaoPreDeterminada()){
            this.duracao = duracao;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.m_strId);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o) {
            return true;
        }
        // null check
        if (o == null) {
            return false;
        }
        // type check and cast
        if (getClass() != o.getClass()) {
            return false;
        }
        // field comparison
        Servico obj = (Servico) o;
        return (Objects.equals(m_strId, obj.m_strId));
    }

    @Override
    public String toString() {
        return String.format("%s - %s - %s - %.2f - Categoria: %s", this.m_strId, this.m_strDescricaoBreve, this.m_strDescricaoCompleta, this.m_dCustoHora, this.m_oCategoria.toString());
    }

}
