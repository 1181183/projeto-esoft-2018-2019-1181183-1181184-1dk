/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.hclc.ListaHorarios;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author 1181183/1181184
 */
public class PedidoPrestacaoServico {
    
    private Cliente m_oCliente;
    private EnderecoPostal m_oEnderecoPostal;
    private String m_strEstadoPedido;
    private int m_intPedidoId;
    private List<PreferenciaHorario> m_lstHorarios = new ArrayList<PreferenciaHorario>();
    private List<DescricaoServicoPedido> m_lstDescricaoServicoPedido = new ArrayList<DescricaoServicoPedido>();
    private List<OutroCusto> m_lstOutroCusto = new ArrayList<OutroCusto>();
    private List<AtribuicaoPedido> m_lstAtribuicoesPedido = new ArrayList<AtribuicaoPedido>();
    private List<OrdemExecucao> m_lstOrdensExecucao = new ArrayList<OrdemExecucao>();

    public PedidoPrestacaoServico( Cliente m_oCliente, EnderecoPostal m_oEnderecoPostal, String m_strEstadoPedido, int m_strPedidoId) {
        this.m_oCliente = m_oCliente;
        this.m_oEnderecoPostal = m_oEnderecoPostal;
        this.m_strEstadoPedido = m_strEstadoPedido;
        this.m_intPedidoId = m_strPedidoId;
    }
    
    public long getPedidoId() {
        return this.m_intPedidoId;
    }
    
    public String getEstadoPedido() {
        return this.m_strEstadoPedido;
    }
    
    public ListaHorarios getListaHorarios() {
        return new ListaHorarios();
    }
    
    public List<AtribuicaoPedido> getListaAtribuicoesPedido() {
        return m_lstAtribuicoesPedido;
    }
    
    public List<OrdemExecucao> getListaOrdensExecucao() {
        return m_lstOrdensExecucao;
    }
    public void verificarAtribuicoes(){
        if(m_lstDescricaoServicoPedido.size() == m_lstAtribuicoesPedido.size()){
            setEstadoPedido(Utils.EstadoPedido.PEDIDO_ENVIADO);
        }
    }
    
    
    public boolean addAtribuicaoPedido(AtribuicaoPedido atPedido) {
        return m_lstAtribuicoesPedido.add(atPedido);
    }
    
    public boolean setEstadoPedido(String oEstadoPedido) {
        this.m_strEstadoPedido = oEstadoPedido;
        if(oEstadoPedido.equalsIgnoreCase(Utils.EstadoPedido.PEDIDO_EXECUTAVEL)){
            for(AtribuicaoPedido atrPedido : m_lstAtribuicoesPedido){
                OrdemExecucao oE = novaOrdemExecucao(m_intPedidoId, atrPedido.getPrestadorServico(), atrPedido.getServico(), atrPedido.getDispDiaria());
                return m_lstOrdensExecucao.add(oE);
            }
        }
        return false;
    }
    
    public OrdemExecucao novaOrdemExecucao(long id, PrestadorServico presServico, Servico servico, DisponibilidadeDiaria DispDiaria){
        return new OrdemExecucao(id, presServico, servico, DispDiaria);
    }
    
    public DescricaoServicoPedido novoDescricaoServicoPedido(String idServ, String desc, String dur) {
        return new DescricaoServicoPedido(idServ, desc, dur);
    }

    public boolean addPedidoServico(String idServ, String desc, String dur) {
        return m_lstDescricaoServicoPedido.add(new DescricaoServicoPedido(idServ, desc, dur));
    }

    public float calculaCusto() {
        float preco = 0;
        return preco;
    }

    private float geraCustosAdicionais() {
        float custosAdicionais = 0;
        //ainda nao sabemos como calcular os custos adicionais
        return custosAdicionais;
    }

    private void addCustosAdicionais() {

    }

    private void removeCustosAdicionais() {

    }

    private void atualizarCustoAdicional() {

    }

    private float getCustoTotal() {
        float custoTotal = 0;
        return custoTotal;
    }

    private void setNumero(int num) {
        //ainda não sabemos como atribuir o número do pedido
        int numeroPedido = num;
    }

    @Override
    public String toString() {
        String str = String.format("%s", this.m_oEnderecoPostal);
        for (DescricaoServicoPedido descServ : this.m_lstDescricaoServicoPedido) {
            str = String.format("%s - %s - %s", descServ.getM_idServ(), descServ.getM_strDescricao(), descServ.getM_strDuracao());
        }
        for (PreferenciaHorario horario : this.m_lstHorarios) {
            str += "\nHorário:\n" + horario.toString();
        }
        return str;
    }

}
