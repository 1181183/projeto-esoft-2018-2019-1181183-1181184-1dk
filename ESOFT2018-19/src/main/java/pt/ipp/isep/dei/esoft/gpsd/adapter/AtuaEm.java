/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.adapter;

/**
 *
 * @author Diogo Oliveira
 */
public class AtuaEm {
    
    public double distancia;

    public AtuaEm(double distancia) {
        this.distancia = distancia;
    }
    
    public double getDistanciaAtuacao(){
        return distancia;
    }
    
}
