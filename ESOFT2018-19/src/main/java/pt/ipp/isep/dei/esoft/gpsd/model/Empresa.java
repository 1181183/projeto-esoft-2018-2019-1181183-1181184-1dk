/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.adapter.AtuaEm;
import pt.ipp.isep.dei.esoft.gpsd.hclc.RegistoPrestadorDeServicos;
import pt.ipp.isep.dei.esoft.gpsd.hclc.RegistoServico;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Empresa {

    private String m_strDesignacao;
    private String m_strNIF;
    private String m_strCusto;
    private String m_strCodPostal;
    private String m_strRaio;
    private String m_strServExt;
    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<Cliente> m_lstClientes;
    private final Set<Categoria> m_lstCategorias;
    private final Set<Servico> m_lstServicos;
    private List<CandidaturaPrestadorDeServicos> m_lstCandidaturaPrestadorServico = new ArrayList<CandidaturaPrestadorDeServicos>();
    
    private Timer myTimer = new Timer();

    public Empresa(String strDesignacao, String strNIF, String strCusto, String strCodPostal, String strRaio, String strServExt) {
        if ((strDesignacao == null) || (strNIF == null) || (strCusto == null) || (strCodPostal == null) || (strRaio == null) || (strServExt == null)
                || (strDesignacao.isEmpty()) || (strNIF.isEmpty()) || (strCusto.isEmpty()) || (strCodPostal.isEmpty()) || (strRaio.isEmpty()) || (strServExt.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;
        this.m_strNIF = strNIF;
        this.m_strCusto = strCusto;
        this.m_strCodPostal = strCodPostal;
        this.m_strRaio = strRaio;
        this.m_strServExt = strServExt;

        this.m_oAutorizacao = new AutorizacaoFacade();

        this.m_lstClientes = new HashSet<>();
        this.m_lstCategorias = new HashSet<>();
        this.m_lstServicos = new HashSet<>();
    }

    public Empresa(String property, String property0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }

    public Set<Categoria> getListaCategorias() {
        return m_lstCategorias;
    }

    // Clientes
    // <editor-fold defaultstate="collapsed">
    public Cliente getClienteByEmail(String strEMail) {
        for (Cliente cliente : this.m_lstClientes) {
            if (cliente.hasEmail(strEMail)) {
                return cliente;
            }
        }

        return null;
    }
    
    public Cliente getCliente(Cliente oCliente){
        return oCliente;
    }

//    public Cliente novoCliente(String strNome, String strNIF, String strTelefone, String strEmail, EnderecoPostal morada) {
//        return new Cliente(strNome, strNIF, strTelefone, strEmail, morada);
//    }
//
//    public boolean registaCliente(Cliente oCliente, String strPwd) {
//        if (this.validaCliente(oCliente, strPwd)) {
//            if (this.m_oAutorizacao.registaUtilizadorComPapel(oCliente.getNome(), oCliente.getEmail(), strPwd, Constantes.PAPEL_CLIENTE)) {
//                return addCliente(oCliente);
//            }
//        }
//        return false;
//    }
//
//    private boolean addCliente(Cliente oCliente) {
//        return m_lstClientes.add(oCliente);
//    }
//
//    public boolean validaCliente(Cliente oCliente, String strPwd) {
//        boolean bRet = true;
//
//        // Escrever aqui o código de validação
//        if (this.m_oAutorizacao.existeUtilizador(oCliente.getEmail())) {
//            bRet = false;
//        }
//        if (strPwd == null) {
//            bRet = false;
//        }
//        if (strPwd.isEmpty()) {
//            bRet = false;
//        }
//        //
//
//        return bRet;
//    }
    // </editor-fold>
    // Serviços
    // <editor-fold defaultstate="collapsed">
    public TipoServico getTiposServicos(TipoServico TipoServ) {
        return TipoServ;
    }

    public Servico getServicoById(String strId) {
        for (Servico serv : this.m_lstServicos) {
            if (serv.hasId(strId)) {
                return serv;
            }
        }

        return null;
    }

    public boolean registaServico(Servico oServico) {
        if (this.validaServico(oServico)) {
            return addServico(oServico);
        }
        return false;
    }

    private boolean addServico(Servico oServico) {
        return m_lstServicos.add(oServico);
    }

    public boolean validaServico(Servico oServico) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    public List<Servico> getServicos() {
        List<Servico> ls = new ArrayList<>();
        ls.addAll(this.m_lstServicos);
        return ls;
    }

    // </editor-fold>
    // Categorias 
    // <editor-fold defaultstate="collapsed">
    public TipoServico getTipoServico(TipoServico Servico) {
        return Servico;
    }

    public RegistoCategorias getRegistoCategorias() {
        return new RegistoCategorias();
    }

    public Categoria novaCategoria(String strCodigo, String strDescricao) {
        return new Categoria(strCodigo, strDescricao);
    }

    public boolean registaCategoria(Categoria oCategoria) {
        if (this.validaCategoria(oCategoria)) {
            return addCategoria(oCategoria);
        }
        return false;
    }

    private boolean addCategoria(Categoria oCategoria) {
        return m_lstCategorias.add(oCategoria);
    }

    public boolean validaCategoria(Categoria oCategoria) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    public List<Categoria> getCategorias() {
        List<Categoria> lc = new ArrayList<>();
        lc.addAll(this.m_lstCategorias);
        return lc;
    }

    // </editor-fold>
// Área Geográfica
// <editor-fold defaultstate="collapsed">

    public RegistoPrestadorDeServicos getRegistoPrestadorDeServicos() {
        return new RegistoPrestadorDeServicos();
    }

    public RegistoAreaGeografica getRegistoAreaGeografica() {
        return new RegistoAreaGeografica();
    }

    public RegistoServico getRegistoServico() {
        return new RegistoServico();
    }
    
    public RegistoCliente getRegistoCliente() {
        return new RegistoCliente();
    }
    public RegistoCandidaturaAPrestadorServicos getRegistoCandidaturaAPrestadorServicos(){
        return new RegistoCandidaturaAPrestadorServicos();
    }
    public ServicoExterno getServicoExterno(ServicoExterno se) {
        return se;
    }
    
    public ServicoExternoAtribuicaoServico getServicoExternoAtribuicao() {
        return ServicoExternoAtribuicaoServico;
    }
    
    public void  criarAfetarPrestadoresServicoTask(float delay, long intervalo){
        final long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime != delay){
            start(intervalo);
        }
    }
    
        final long startTime = System.currentTimeMillis();
        TimerTask AfetarPrestadoresServicoTask = new TimerTask() {
            public void run() {
                RegistoCliente rC = getRegistoCliente();
                List<PedidoPrestacaoServico> listaPedidosTotaisSubmetidos = rC.getListaTodosPedidosSubmetidos();
                ServicoExterno sExterno;
                List<AtribuicaoPedido> listaAtribuicaoPedidos = sExterno.atribuicaoPedidos(listaPedidosTotaisSubmetidos);
                rC.associarAtribuicaoAPedido(listaAtribuicaoPedidos);
            }
        };
   
   
    public void start(long intervalo) {
        myTimer.schedule(AfetarPrestadoresServicoTask, intervalo);
    }
    
    public List<String> getListaFormatoExportacao() {
        ArrayList<String> listaFormatoExportacao = new ArrayList<>();
        listaFormatoExportacao.add(".CSV");
        listaFormatoExportacao.add(".XML");
        listaFormatoExportacao.add(".EXCEL");
        return listaFormatoExportacao;
    }
}
