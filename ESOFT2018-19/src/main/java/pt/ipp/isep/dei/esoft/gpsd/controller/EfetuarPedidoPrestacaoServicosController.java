/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.gpsd.hclc.ListaHorarios;
import pt.ipp.isep.dei.esoft.gpsd.hclc.RegistoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.model.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.DescricaoServicoPedido;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.PreferenciaHorario;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoCategorias;
import pt.ipp.isep.dei.esoft.gpsd.model.Servico;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author tarde
 */
public class EfetuarPedidoPrestacaoServicosController {

    private Empresa m_oEmpresa;
    private Cliente m_oCliente;
    private PedidoPrestacaoServico m_oPedidoServico;

    public RegistoCategorias getRegistoCategorias() {
        return this.m_oEmpresa.getRegistoCategorias();
    }
    
    public RegistoServico getRegistoServicos() {
        return this.m_oEmpresa.getRegistoServico();
    }
    
    public ListaHorarios getListaHorarios() {
        return this.m_oPedidoServico.getListaHorarios();
    }
    
    public float getCustoTotal() {
        return this.m_oPedidoServico.calculaCusto();
    }
    
    public void setEnderecoPostal(String end, String codPostal, String local) {
        this.m_oCliente.getListaEnderecoPostal().novoEnderecoPostal(local, codPostal, local);
    }
    
     public boolean novoPedido(String idServ, String desc, String dur, String horaInicio, String dataI, Cliente oCliente, EnderecoPostal oEnderecoPostal, String oEstadoPedido, int oPedidoId)
    {
        try
        {
           PreferenciaHorario horario = m_oPedidoServico.getListaHorarios().novoPreferenciaHorario(horaInicio, dataI);
           DescricaoServicoPedido descServ = m_oPedidoServico.novoDescricaoServicoPedido(idServ, desc, dur);
            this.m_oPedidoServico = this.m_oCliente.getListaPrestacaoServicos().novoPedidoServico(oCliente, oEnderecoPostal, oEstadoPedido, oPedidoId);
            return this.m_oCliente.getListaPrestacaoServicos().validaPedidoPrestacaoServicos(m_oPedidoServico);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oPedidoServico = null;
            return false;
        }
    }
    public boolean registaPedidoServico() {
        return this.m_oCliente.getListaPrestacaoServicos().registaPedido(m_oPedidoServico);
    }
    
    public boolean valida() {
        return this.m_oCliente.getListaPrestacaoServicos().validaPedidoPrestacaoServicos(m_oPedidoServico);
    }
    

    public String getPedidoServicoString() {
                return this.m_oPedidoServico.toString();
    }
}
