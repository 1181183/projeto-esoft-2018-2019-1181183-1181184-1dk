/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.Objects;

/**
 *
 * @author paulomaio
 */
public class EnderecoPostal {

    private String local;
    private CodigoPostal codPostal;
    private String localidade;

    public EnderecoPostal(String local, String codPostal, String localidade) {
        if ((local == null) || (codPostal == null) || (localidade == null)
                || (local.isEmpty()) || (codPostal.isEmpty()) || (localidade.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.local = local;
        setDados(codPostal);
        this.localidade = localidade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.local, this.codPostal, this.localidade);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o) {
            return true;
        }
        // null check
        if (o == null) {
            return false;
        }
        // type check and cast
        if (getClass() != o.getClass()) {
            return false;
        }
        // field comparison
        EnderecoPostal obj = (EnderecoPostal) o;
        return (Objects.equals(local, obj.local)
                && Objects.equals(codPostal, obj.codPostal)
                && Objects.equals(localidade, obj.localidade));
    }

    @Override
    public String toString() {
        return String.format("%s \n %s - %s", this.local, this.codPostal, this.localidade);
    }

    public void setDados(String codPostal) {
        this.codPostal = new CodigoPostal(codPostal);
    }

}
