/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Diogo Oliveira
 */
public class RegistoCandidaturaAPrestadorServicos {
    List<CandidaturaPrestadorDeServicos> m_lstCandidaturaPrestadorServico;
    
    public RegistoCandidaturaAPrestadorServicos() {
        m_lstCandidaturaPrestadorServico = new ArrayList<>();
    }
    
    public CandidaturaPrestadorDeServicos getCandidaturaByNif(String strNif) {
        for(CandidaturaPrestadorDeServicos candPresServ : this.m_lstCandidaturaPrestadorServico) {
            if(candPresServ.hasNif(strNif)) {
                return candPresServ;
            }
        }
        return null;
    }
    
    public CandidaturaPrestadorDeServicos novaCandidaturaPrestadorServicos(String strNif, long nMecanografico, String strNomeComp, String strNomeAbv, String strTel, String strEMail, EnderecoPostal oMorada, HabilitacaoAcademica aHabAcademica, HabilitacaoProfissional aHabProfissional, DocumentosComprovativos oDocComprovativo, String catId) {
        return new CandidaturaPrestadorDeServicos(strNif, nMecanografico, strNomeComp, strNomeAbv, strTel, strEMail, oMorada, aHabAcademica, aHabProfissional, oDocComprovativo, catId);
    }
    
    private boolean registaCandidaturaPrestadorServico(CandidaturaPrestadorDeServicos c_CanPresServ) throws Exception {
        if (this.validaCandidaturaPrestadorServiço(c_CanPresServ)) {
            return this.addCandidaturaPrestadorServiço(c_CanPresServ);
        }
        return false;
    }
    
    private boolean addCandidaturaPrestadorServiço(CandidaturaPrestadorDeServicos c_CanPresServ) {
        return m_lstCandidaturaPrestadorServico.add(c_CanPresServ);
    }
    
    public boolean validaCandidaturaPrestadorServiço(CandidaturaPrestadorDeServicos c_CanPresServ) {
        boolean bRet = true;
        String strEmail = c_CanPresServ.getEmail();
        String strNif = c_CanPresServ.getNif();
            for (CandidaturaPrestadorDeServicos candPresServicos: m_lstCandidaturaPrestadorServico) {
                if(candPresServicos.getNif().equalsIgnoreCase(strNif) || candPresServicos.getEmail().equalsIgnoreCase(strEmail)) {
                    bRet=false;
                }
            }
        return bRet;
    }
    
}
