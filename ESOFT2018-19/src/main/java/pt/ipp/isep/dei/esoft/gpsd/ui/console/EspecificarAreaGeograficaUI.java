
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.EspecificarAreaGeograficaController;
import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

public class EspecificarAreaGeograficaUI {
    private EspecificarAreaGeograficaController m_controller;     
    
    public EspecificarAreaGeograficaUI() {
        m_controller = new EspecificarAreaGeograficaController();
        AreaGeografica v_AreaGeo;
        
    }
    
    public void run() throws Exception
    {
        System.out.println("\nEspecificar Área Geográica:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaAreaGeografica()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    private boolean introduzDados(){
        String strLocalizacao = Utils.readLineFromConsole("Localização: ");
        String strCusto = Utils.readLineFromConsole("Custo: ");
        String strRaio = Utils.readLineFromConsole("Raio: ");
        
        return m_controller.novaAreaGeografica(strLocalizacao, strCusto, strRaio);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nÁrea Geográfica:\n" + m_controller.getAreaGeograficaString());
    }
}
   