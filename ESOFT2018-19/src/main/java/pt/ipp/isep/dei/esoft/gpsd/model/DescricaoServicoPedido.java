/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author Rita
 */
public class DescricaoServicoPedido {

    private String m_idServ;
    private String m_strDescricao;
    private String m_strDuracao;

    public DescricaoServicoPedido(String m_idServ, String m_strDescricao, String m_strDuracao) {
        this.m_idServ = m_idServ;
        this.m_strDescricao = m_strDescricao;
        this.m_strDuracao = m_strDuracao;
    }

    /**
     * @return the m_idServ
     */
    public String getM_idServ() {
        return m_idServ;
    }

    /**
     * @return the m_strDescricao
     */
    public String getM_strDescricao() {
        return m_strDescricao;
    }

    /**
     * @return the m_strDuracao
     */
    public String getM_strDuracao() {
        return m_strDuracao;
    }

}
