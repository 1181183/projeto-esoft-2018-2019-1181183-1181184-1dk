/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.gpsd.hclc.ListaDisponibilidadeDiaria;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.DisponibilidadeDiaria;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.PrestadorServico;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author Rita
 */
public class IndicarDisponibilidadeDiariaController {

    private Empresa m_oEmpresa;
    private PrestadorServico m_oPrestadorServico;
    private DisponibilidadeDiaria m_oDisponibilidadeDiaria;

    public IndicarDisponibilidadeDiariaController() {
        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_PRESTADOR_SERVICO)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
        //this.m_oPrestadorServico = AplicacaoGPSD.getInstance().getSessaoAtual();

    }

    public ListaDisponibilidadeDiaria getListaDisponibilidadeDiaria() {
        return this.m_oPrestadorServico.getListaDisponibilidadeDiaria();
    }

    public boolean novoDisponibilidadeDiaria(String horaInicio, String horaFim) {
        try {
            this.m_oDisponibilidadeDiaria = this.getListaDisponibilidadeDiaria().novaDisponibilidadeDiaria(horaInicio, horaFim);
            return this.getListaDisponibilidadeDiaria().validaDisponibilidadeDiaria(this.m_oDisponibilidadeDiaria);
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oDisponibilidadeDiaria = null;
            return false;
        }
    }

    public boolean registaDisponibilidadeDiaria() {
        return this.getListaDisponibilidadeDiaria().registaDisponibilidadeDiaria(m_oDisponibilidadeDiaria);
    }

    public String getDisponibilidadeDiariatoString() {
        return this.m_oDisponibilidadeDiaria.toString();

    }

}
