/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.gpsd.model.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.ListaEnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author paulomaio
 */
public class RegistarClienteController
{
    private AplicacaoGPSD m_oApp;
    private Empresa m_oEmpresa;
    private Cliente m_oCliente;
    private String m_strPwd;
    private String m_strCat;
    
    public RegistarClienteController()
    {
        this.m_oApp = AplicacaoGPSD.getInstance();
        this.m_oEmpresa = m_oApp.getEmpresa();
    }
    
    
    public boolean novoCliente(String strNome, String strNIF, String strTelefone, String strEmail, String strPwd, String strLocal, String strCodPostal, String strLocalidade)
    {
        try
        {
            this.m_strPwd = strPwd;
            EnderecoPostal morada = new EnderecoPostal(strLocal, strCodPostal, strLocalidade);
            this.m_oCliente = this.m_oEmpresa.getRegistoCliente().novoCliente(strNome, strNIF, strTelefone, strEmail, morada, strCodPostal, strPwd, strLocal);
            return this.m_oEmpresa.getRegistoCliente().validaCliente(m_oCliente, strPwd);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCliente = null;
            return false;
        }
    }
    
    public boolean addEnderecoPostal(String strLocal, String strCodPostal, String strLocalidade)
    {
        if (this.m_oCliente != null)
        {
            try
            {
                EnderecoPostal morada = m_oCliente.getListaEnderecoPostal().novoEnderecoPostal(strLocal, strCodPostal, strLocalidade);
                return this.m_oCliente.getListaEnderecoPostal().addEnderecoPostal(morada);
            }
            catch(RuntimeException ex)
            {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } 
        return false;
    }
    
    public boolean registaCliente()
    {
        return this.m_oEmpresa.getRegistoCliente().registaCliente(this.m_oCliente, this.m_strPwd);
    }

    public String getClienteString()
    {
        return this.m_oCliente.toString();
    }
    
}