/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.gpsd.model.CandidaturaPrestadorDeServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.PrestadorServico;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoAreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoCategorias;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author Rita
 */
public class RegistarPrestadorServicosController {

    private Empresa m_oEmpresa;
    private PrestadorServico m_oPrestadorServico;
    private String m_strPwd;

    public RegistarPrestadorServicosController() {
        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_FRH)) {
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
        //this.m_oPrestadorServico = AplicacaoGPSD.getInstance().getSessaoAtual();

    }

    public RegistoAreaGeografica getRegistoAreaGeografica() {
        return this.m_oEmpresa.getRegistoAreaGeografica();
    }

    public RegistoCategorias getRegistoCategoria() {
        return this.m_oEmpresa.getRegistoCategorias();
    }

    public boolean novoPrestadorServico(String nif, long nMecanografico, String nomeComp, String nomeAbv, String email, String pwd) {
        try {
            m_strPwd = pwd;
            this.m_oPrestadorServico = this.m_oEmpresa.getRegistoPrestadorDeServicos().novoPrestadorServico(nif, nMecanografico, nomeComp, nomeAbv, email);
            return this.m_oEmpresa.getRegistoPrestadorDeServicos().validaPrestadorServico(this.m_oPrestadorServico, pwd);
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oPrestadorServico = null;
            return false;
        }
    }

    public boolean registaPrestadorServico() {
        return this.m_oEmpresa.getRegistoPrestadorDeServicos().registaPrestadorServico(m_oPrestadorServico, m_strPwd);
    }

    public String getPrestadorServicoString() {
        return this.m_oPrestadorServico.toString();
    }
    
    public boolean getCandidaturaByNif(String nif){
        if(!this.m_oEmpresa.getRegistoCandidaturaAPrestadorServicos().getCandidaturaByNif(nif).equals(null)){
            CandidaturaPrestadorDeServicos candidatura = this.m_oEmpresa.getRegistoCandidaturaAPrestadorServicos().getCandidaturaByNif(nif);
            long nMecanografico = candidatura.getNumeroMecanografico();
            String nomeComp = candidatura.getNomeCompleto();
            String nomeAbv = candidatura.getNomeAbreviado();
            String email = candidatura.getEmail();
            return novoPrestadorServico(nif, nMecanografico, nomeComp, nomeAbv, email, nif);
        }
        return false;
    }

}
