/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.model.AtribuicaoPedido;
import pt.ipp.isep.dei.esoft.gpsd.model.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.OrdemExecucao;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoCliente;

/**
 *
 * @author Diogo Oliveira
 */
public class DecidirSobrePeriodoPropostoParaRealizacaoDeServicosController {
    
    private Empresa empresa = AplicacaoGPSD.getInstance().getEmpresa();
    RegistoCliente rC = empresa.getRegistoCliente();
    Cliente cli;
    PedidoPrestacaoServico pedidoCliente;
    
    public DecidirSobrePeriodoPropostoParaRealizacaoDeServicosController(String strEmail, long id) {
        cli = rC.getClienteByEmail(strEmail);
        pedidoCliente = cli.getPedidoPrestacaoServicosById(id);
    } 
    
    public List<AtribuicaoPedido> getAtribuicoesPedido(){
        return pedidoCliente.getListaAtribuicoesPedido();
    }
    
    public boolean setEstadoPedido(String estado) {
        return pedidoCliente.setEstadoPedido(estado);
    }
    
    public List<OrdemExecucao> getListaOrdensExecucao(){
        return pedidoCliente.getListaOrdensExecucao();
    }
}
