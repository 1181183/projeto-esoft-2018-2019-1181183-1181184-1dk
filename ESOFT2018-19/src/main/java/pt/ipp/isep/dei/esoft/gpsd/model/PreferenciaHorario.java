/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author tarde
 */
public class PreferenciaHorario {

    private String m_strHoraInicio;
    private String m_strDataI;

    public PreferenciaHorario(String horaInicio, String dataI) {
         if ( (horaInicio == null) || (dataI == null) ||
                (horaInicio.isEmpty())|| (dataI.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strHoraInicio = horaInicio;
        this.m_strDataI = dataI;
    }
    
    public String toString() {
        return String.format("Hora de início: %s \n Data de realização do serviço: %s", this.m_strHoraInicio, this.m_strDataI);
    }

}
