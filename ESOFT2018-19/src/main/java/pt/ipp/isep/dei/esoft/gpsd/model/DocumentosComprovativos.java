/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.Objects;

public class DocumentosComprovativos {

    private String m_objDocComp;

    public DocumentosComprovativos() {
        //ainda não há informações acerca do que colocar nos documentos comprovativos
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.m_objDocComp);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o) {
            return true;
        }
        // null check
        if (o == null) {
            return false;
        }
        // type check and cast
        if (getClass() != o.getClass()) {
            return false;
        }
        // field comparison
        DocumentosComprovativos obj = (DocumentosComprovativos) o;
        return (Objects.equals(m_objDocComp, obj.m_objDocComp));
    }

    @Override
    public String toString() {
        return (String) m_objDocComp;
    }

    boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
