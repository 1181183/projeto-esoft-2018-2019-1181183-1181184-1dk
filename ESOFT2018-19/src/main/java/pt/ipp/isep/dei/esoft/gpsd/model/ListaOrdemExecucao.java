/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1181183/1181184
 */
public class ListaOrdemExecucao {
    
    private final List<OrdemExecucao> m_listaOrdemExecucao;
    
    public ListaOrdemExecucao() {
        this.m_listaOrdemExecucao = new ArrayList<>();
    }
   
    public OrdemExecucao novaOrdemExecucao(long id, PrestadorServico prestador, Servico servico, DisponibilidadeDiaria dispDiaria){
        return new OrdemExecucao(id, prestador, servico, dispDiaria);
    }

    public boolean addOrdemExecucao(OrdemExecucao ordemExecucao){
        return m_listaOrdemExecucao.add(ordemExecucao);
    }
    
    public boolean removeOrdemExecucao(OrdemExecucao ordemExecucao){
        return m_listaOrdemExecucao.remove(ordemExecucao);
    }
    
    public boolean validaEnderecoPostal(String horaInicio, String horaFim){
        boolean bRet = true;
        //ainda não sabemos como validar a disponibilidade diária

        return bRet;    
    }
}
