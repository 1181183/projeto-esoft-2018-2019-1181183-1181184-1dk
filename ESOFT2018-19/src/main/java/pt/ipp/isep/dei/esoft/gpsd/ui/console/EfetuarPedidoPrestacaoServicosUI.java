/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import com.sun.security.ntlm.Client;
import java.time.LocalDate;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.controller.EfetuarPedidoPrestacaoServicosController;
import pt.ipp.isep.dei.esoft.gpsd.hclc.ListaHorarios;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.model.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.PreferenciaHorario;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoCategorias;
import pt.ipp.isep.dei.esoft.gpsd.model.Servico;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author 1181183/1181184
 */
public class EfetuarPedidoPrestacaoServicosUI {
    
    private ListaHorarios m_oListaHorarios;
    private Empresa m_oEmpresa;
    private RegistoCategorias m_oRegistoCat;
    private EfetuarPedidoPrestacaoServicosController m_controller;
    
    private Cliente oCliente;
    private EnderecoPostal oEnderecoPostal;
    private String oEstadoPedido;
    private int oPedidoId;
    private PreferenciaHorario prefHorario;
    
    public EfetuarPedidoPrestacaoServicosUI() {
        m_controller = new EfetuarPedidoPrestacaoServicosController();
    }

    public void run() {
        System.out.println("\nEfetuar Pedido para Prestação de Serviços:");

        if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaPedidoServico()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        List<Categoria> lc = m_oRegistoCat.getCategorias();

        String catId = "";
        Categoria c = (Categoria) Utils.apresentaESeleciona(lc, "Selecione a categoria a que o serviço pertence:");
        if (c != null) {
            catId = c.getCodigo();
        }

        List<Servico> ls = m_oEmpresa.getServicos();

        String servId = "";
        Servico s = (Servico) Utils.apresentaESeleciona(lc, "Selecione o serviço que pretende:");
        if (s != null) {
            servId = s.getId();
        }

        String strDescricao = Utils.readLineFromConsole("Descrição do serviço: ");
        String strDuracao = "";
        if (s.isDuracaoRequerida()) {
            strDuracao = Utils.readLineFromConsole("Duração do serviço: ");
        } else {
            strDuracao = s.calculoDuracao();
        }

        LocalDate data = Utils.getDateToday();

        System.out.println("\nIntroduza pelo menos um horário:");
        int count = 0;
        do {

            String horaInicio = Utils.readLineFromConsole("Hora ínicio: ");
            String dataP = Utils.readLineFromConsole("Data de prestação de serviços: ");
            oEstadoPedido = Utils.EstadoPedido.PEDIDO_SUBMETIDO;
            oPedidoId++;
            if (count == 0) {
                if (m_controller.novoPedido(servId, dataP, dataP, horaInicio, dataP, oCliente, oEnderecoPostal, oEstadoPedido, oPedidoId)) {
                    count++;
                } else {
                    return false;
                }
            } else {
                if (!m_oListaHorarios.addHorario(prefHorario)) {
                    System.out.println("Ocorreu um erro ao adicionar o horário.");
                }
            }
        } while ((count == 0) || Utils.confirma("Pretende introduzir outro Horário (S/N)?"));
        return true;
    }

    private void apresentaDados() {
        System.out.println("\n Cliente:\n" + m_controller.getPedidoServicoString());
    }
}
