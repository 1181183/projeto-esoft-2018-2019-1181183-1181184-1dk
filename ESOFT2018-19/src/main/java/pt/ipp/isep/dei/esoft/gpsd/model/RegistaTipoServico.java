/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;

/**
 *
 * @author Diogo Oliveira
 */
public class RegistaTipoServico {
        
    private String m_strDescB;
    private String m_strDescC;
    private String m_strCatId;
    private String m_strServico;
    private float m_floatCusto;
    
    private ArrayList<String>tiposServico;
    
    private static final String servico_Fixo = "Fixo";
    private static final String servico_Limitado = "Limitado";
    private static final String servico_Expansivel = "Expansivel";
    
    public ArrayList<String> getTiposServico(){
        if(tiposServico.isEmpty()){
            tiposServico.add(servico_Fixo);
            tiposServico.add(servico_Limitado);
            tiposServico.add(servico_Expansivel);
        }
        return tiposServico;
    }
    
    public boolean validaServico(Servico servico){
        //ainda não sabemos como validar o prestador de serviços
        return true;
    }
    
    public void addServico(String m_strServico){
        this.m_strServico = m_strServico;
    }
}
