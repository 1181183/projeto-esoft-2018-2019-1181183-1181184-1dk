/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.hclc;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.PrestadorServico;
import pt.ipp.isep.dei.esoft.gpsd.model.RegistoAreaGeografica;

/**
 *
 * @author Rita
 */
public class RegistoPrestadorDeServicos {

    private final AutorizacaoFacade m_oAutorizacao;
    private final List<PrestadorServico> m_lstPrestadorServico;

    public RegistoPrestadorDeServicos() {
        this.m_lstPrestadorServico = new ArrayList<>();
        this.m_oAutorizacao = new AutorizacaoFacade();
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }
    
    public PrestadorServico getPrestadorServicoByEmail(String strEmail){
        for(PrestadorServico prestador : m_lstPrestadorServico){
            if(prestador.getEmail().equalsIgnoreCase(strEmail)){
                return prestador;
            }
        }
        return null;
    }
    public PrestadorServico novoPrestadorServico(String m_strNif, long m_longNumMecanografico, String m_strNomeCompleto, String m_strNomeAbreviado, String m_strEmailInstitucional) {
        return new PrestadorServico(m_strNif, m_longNumMecanografico, m_strNomeCompleto, m_strNomeAbreviado, m_strEmailInstitucional);
    }


    private boolean addPrestadorServico(PrestadorServico oPrestadorServico) {
        return m_lstPrestadorServico.add(oPrestadorServico);
    }
    
    private boolean removePrestadorServico(PrestadorServico oPrestadorServico) {
        return m_lstPrestadorServico.remove(oPrestadorServico);
    }

    public boolean validaPrestadorServico(PrestadorServico oPrestadorServico, String strPwd) {
        boolean bRet = true;
        //ainda não sabemos como validar o prestador de serviços
        if (this.m_oAutorizacao.existeUtilizador(oPrestadorServico.getEmail())) {
            bRet = false;
        }
        if (strPwd == null) {
            bRet = false;
        }
        if (strPwd.isEmpty()) {
            bRet = false;
        }
        return bRet;
    }

    public boolean registaPrestadorServico(PrestadorServico oPrestadorServico, String strPwd) {
        if (this.validaPrestadorServico(oPrestadorServico, strPwd)) {
            if (this.m_oAutorizacao.registaUtilizadorComPapel(oPrestadorServico.getNome(), oPrestadorServico.getEmail(), strPwd, Constantes.PAPEL_PRESTADOR_SERVICO)) {
                return addPrestadorServico(oPrestadorServico);
            }
        }
        return false;
    }
}
