/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.hclc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.model.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.PreferenciaHorario;
import pt.ipp.isep.dei.esoft.gpsd.model.Servico;

/**
 *
 * @author Rita
 */
public class ListaPedidoPrestacaoServicos {

    List<PedidoPrestacaoServico> m_lstPedidoPrestacaoServico;
    int cont;

    public ListaPedidoPrestacaoServicos() {
        m_lstPedidoPrestacaoServico = new ArrayList<PedidoPrestacaoServico>();
        cont++;
    }
    
    public PedidoPrestacaoServico novoPedidoServico( Cliente oCliente, EnderecoPostal oEnderecoPostal, String oEstadoPedido, int oPedidoId) {
        return new PedidoPrestacaoServico(oCliente, oEnderecoPostal, oEstadoPedido, oPedidoId);
    }
    
    public List<PedidoPrestacaoServico> getListaPedidos() {
        return m_lstPedidoPrestacaoServico;
    }
    
    public boolean verificaPedidoPrestacaoServicos(PedidoPrestacaoServico p_pServ) {
        boolean bRet = true;
        //Ainda não sabemos como verificar o pedido de serviço...
        return bRet;
    }

    public boolean validaPedidoPrestacaoServicos(PedidoPrestacaoServico p_pServ) {
        boolean bRet = true;
        //Ainda não sabemos como validar o pedido de serviço...
        return bRet;
    }

    private boolean addPedido(PedidoPrestacaoServico pServ) {
        return m_lstPedidoPrestacaoServico.add(pServ);
    }
    
    private boolean removePedido(PedidoPrestacaoServico pServ) {
        return m_lstPedidoPrestacaoServico.remove(pServ);
    }

    public boolean registaPedido(PedidoPrestacaoServico pServ) {
        if (this.validaPedidoPrestacaoServicos(pServ)) {
            return this.addPedido(pServ);
        }
        return false;
    }
    
    public int geraNumPedido() {
        return cont;
    }
    
    
    
    

}
