/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.hclc;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.model.DisponibilidadeDiaria;

/**
 *
 * @author Rita
 */
public class ListaDisponibilidadeDiaria {

    private final List<DisponibilidadeDiaria> m_lstDisponibilidadeDiaria;

    public ListaDisponibilidadeDiaria() {
        this.m_lstDisponibilidadeDiaria = new ArrayList<>();
    }

    public DisponibilidadeDiaria novaDisponibilidadeDiaria(String horaInicio, String horaFim) {
        return new DisponibilidadeDiaria(horaInicio, horaFim);
    }

    private boolean addDisponibilidadeDiaria(DisponibilidadeDiaria dispDiaria) {
        return m_lstDisponibilidadeDiaria.add(dispDiaria);
    }

    private boolean removeDisponibilidadeDiaria(DisponibilidadeDiaria dispDiaria) {
        return m_lstDisponibilidadeDiaria.remove(dispDiaria);
    }

    public boolean validaDisponibilidadeDiaria(DisponibilidadeDiaria dispDiaria) {
        boolean bRet = true;
        //ainda não sabemos como validar a disponibilidade diária

        return bRet;
    }

    public boolean registaDisponibilidadeDiaria(DisponibilidadeDiaria dispDiaria) {
        if (this.validaDisponibilidadeDiaria(dispDiaria)) {
            return addDisponibilidadeDiaria(dispDiaria);
        } else {
            return false;
        }
    }

}
