/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author Diogo Oliveira
 */
public class TipoServico {
    
    private String designacao;
    private String idTipoServico;

    public TipoServico(String designacao, String idTipoServico) {
        this.designacao = designacao;
        this.idTipoServico = idTipoServico;
    }
    
    
    public Object novoServico(String id, String descB, String descC, double dcustoHora, Categoria catId) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Class <?> classe = Class.forName("pt.ipp.isep.dei.esoft.gpsd.model." + this.designacao);
        Constructor <?> construtor = classe.getConstructor(String.class, String.class, String.class, double.class, Categoria.class);
        Object servico = construtor.newInstance(id, descB, descC, dcustoHora, catId);
        return servico;
    }
    
    public boolean hasId(String strId){
        return this.idTipoServico.equals(strId);
    }
    
    public String getCodigo(){
        return this.idTipoServico;
    }
}
