/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.gpsd.controller.AplicacaoGPSD;
import pt.ipp.isep.dei.esoft.gpsd.hclc.ListaDisponibilidadeDiaria;

/**
 *
 * @author Rita
 */
public class PrestadorServico {
    
    private String m_strNif;
    private long m_longNumMecanografico;
    private String m_strNomeCompleto;
    private String m_strNomeAbreviado;
    private String m_strEmailInstitucional;
    private final List<Categoria> m_lstCategorias;
    private final List<AreaGeografica> m_lstAreasGeograficas;
    private final List<OrdemExecucao> m_lstOrdemExecucao;
    
    public PrestadorServico(String nif, long m_longNumMecanografico, String m_strNomeCompleto, String m_strNomeAbreviado, String m_strEmailInstitucional) {
        this.m_strNif = m_strNif;
        this.m_longNumMecanografico = m_longNumMecanografico;
        this.m_strNomeCompleto = m_strNomeCompleto;
        this.m_strNomeAbreviado = m_strNomeAbreviado;
        this.m_strEmailInstitucional = m_strEmailInstitucional;
        this.m_lstCategorias = new ArrayList<>();
        this.m_lstAreasGeograficas = new ArrayList<>();
        this.m_lstOrdemExecucao = new ArrayList<>();
    }

//    public boolean validaCategoria(Categoria oCategoria) {
//        boolean bRet = true;
//        //ainda não sabemos como validar a categoria
//
//        return bRet;
//    }
//    
//    public boolean validaAreaGeografica(AreaGeografica aGeo) {
//        boolean bRet = true;
//        //ainda não sabemos como validar a categoria
//        return bRet;
//    }
    
    public String getNif() {
        return this.m_strNif;
    }
    
    public String getNome() {
        return this.m_strNomeCompleto;
    }
    
    public String getEmail() {
        return this.getEmail();
    }
   
    public List<OrdemExecucao> getListaOrdemServico(String dataInicio, String dataFim){
        return m_lstOrdemExecucao;
    }
    
    private boolean addAreaGeografica(AreaGeografica aGeo) {
        return m_lstAreasGeograficas.add(aGeo);
    }

    private boolean removeAreaGeografica(AreaGeografica aGeo) {
        return m_lstAreasGeograficas.remove(aGeo);
    }

    private boolean addCategoria(Categoria oCategoria) {
        return m_lstCategorias.add(oCategoria);
    }

    private boolean removeCategoria(Categoria oCategoria) {
        return m_lstCategorias.remove(oCategoria);
    }

    public ListaDisponibilidadeDiaria getListaDisponibilidadeDiaria() {
        return new ListaDisponibilidadeDiaria();
    }
    
    public ListaOrdemExecucao getListaOrdemExecucao() {
        return new ListaOrdemExecucao();
    }
    @Override
    public String toString() {
        String str = String.format("%s - %s - %s - %s", this.m_longNumMecanografico, m_strNomeCompleto, m_strNomeAbreviado, m_strEmailInstitucional);
        for (Categoria categoria : m_lstCategorias) {
            str += "\nCategoria:\n" + categoria.toString();
        }
        for (AreaGeografica aGeo : m_lstAreasGeograficas) {
            str += "\nÁrea Geográfica:\n" + aGeo.toString();
        }
        return str;
    }

}
